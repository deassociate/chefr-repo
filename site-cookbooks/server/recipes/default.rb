
username = node[:user][:name]

user username do
  password node[:user][:password]
end

group "wheel" do
  action  :modify
  members username
  append  true
end

template "sudoers" do
  owner  "root"
  group  "root"
  path   "/etc/sudoers"
  mode   "0440"
  source "sudoers.erb"
end

%w{git emacs}.each do |pkg|
  package pkg do
    action :install
  end
end

directory "/home/#{username}/.emacs.d" do
  owner  username
  group  username
  action :create
end

template "init.el" do
  owner  username
  group  username
  mode   "0644"
  path   "/home/#{username}/.emacs.d/init.el"
  source "init.el.erb"
end

directory "/home/#{username}/.ssh" do
  owner  username
  group  username
  mode  "0700"
  action :create
end

cookbook_file "/home/#{username}/.ssh/authorized_keys" do
  owner  username
  group  username
  mode   00600
  source "id_rsa.pub"
end

git "/home/#{username}/.rbenv" do
  repository "https://github.com/sstephenson/rbenv.git"
  revision   "master"
  user       username
  group      username
  action     :sync
end

directory "/home/#{username}/.rbenv/plugins" do
  owner  username
  group  username
  action :create
end

git "/home/#{username}/.rbenv/plugins/ruby-build" do
  repository "https://github.com/sstephenson/ruby-build.git"
  revision   "master"
  user       username
  group      username
  action     :sync
end

%w{.bash_profile .bashrc}.each do |filename|
  template filename do
    owner  username
    group  username
    mode   "0644"
    path   "/home/#{username}/#{filename}"
    source "#{filename}.erb"
  end
end

template "rbenv.sh" do
  owner  username
  group  username
  path   "/home/#{username}/rbenv.sh"
  source "rbenv.sh.erb"
end

package "openssl-devel" do
  action :install
end

bash "rbenv install 2.0.0-p353" do
  user   username
  cwd    "/home/#{username}"
  code   "source rbenv.sh; rbenv install 2.0.0-p353"
  action :run
  not_if { ::File.exists? "/home/#{username}/.rbenv/versions/2.0.0-p353" }
end

bash "rbenv rehash" do
  user   username
  cwd    "/home/#{username}"
  code   "source rbenv.sh; rbenv rehash"
  action :run
end

bash "rbenv global 2.0.0-p353" do
  user   username
  cwd    "/home/#{username}"
  code   "source rbenv.sh; rbenv global 2.0.0-p353"
  action :run
end

bash "bundler" do
  user   username
  cwd    "/home/#{username}"
  code   "source rbenv.sh; gem install bundler"
  action :run
  not_if { ::File.exists? "/home/#{username}/.rbenv/shims/bundle" }
end

#bash "unicorn" do
#  user   username
#  cwd    "/home/#{username}"
#  code   "source rbenv.sh; gem install unicorn"
#  action :run
#  not_if { ::File.exists? "/home/#{username}/.rbenv/shims/unicorn" }
#end

bash "rbenv rehash" do
  user   username
  cwd    "/home/#{username}"
  code   "source rbenv.sh; rbenv rehash"
  action :run
end

template "iptables" do
  path   "/etc/sysconfig/iptables"
  source "iptables.erb"
end

service "iptables" do
  action [:start, :enable]
end

# vsftpdインストール
package "vsftpd" do
  action :install
end

#vsftpd.confの設定
template "vsftpd.conf" do
  owner  "root"
  group  "root"
  mode   "0644"
  path   "/etc/vsftpd/vsftpd.conf"
  source "vsftpd.erb"
end

template "vsftpd.chroot_list" do
  owner  "root"
  group  "root"
  mode   "0644"
  path   "/etc/vsftpd/chroot_list"
  source "vsftpd.chroot_list.erb"
end

template "vsftpd.user_list" do
  owner  "root"
  group  "root"
  mode   "0644"
  path   "/etc/vsftpd/user_list"
  source "vsftpd.user_list.erb"
end

# vsftpd利用ディレクトリ作成
directory "/etc/skel" do
  owner  "root"
  group  "root"
  mode "0755"
  action :create
end

# vsftpd利用ディレクトリ作成
directory "/etc/skel/etc" do
  owner  "root"
  group  "root"
  mode "0755"
  action :create
end

bash "cp /etc/localtime" do
  user   "root"
  cwd    "/etc/skel/etc"
  code   "cp /etc/localtime /etc/skel/etc"
  action :run
end

template "vsftpd.pem" do
  owner  "root"
  group  "root"
  mode   "0644"
  path   "/etc/pki/tls/certs/vsftpd.pem"
  source "vsftpd.pem.erb"
end

service "vsftpd" do
  action [:enable, :start]
end

# vsftpd自動起動設定
bash "cp /etc/localtime" do
  user   "root"
  cwd    "/home/#{username}"
  code   "chkconfig vsftpd on"
  action :run
end

# Webルートディレクトリ
directory "/var/www" do
  owner  "root"
  group  "root"
  mode "0777"
  action :create
end

# Webルートディレクトリ
directory "/var/www/html" do
  owner  "root"
  group  "root"
  mode "0777"
  action :create
end

bash "chmod directory" do
  user "root"
  group "root"
  cwd "/"
  code <<-EOC
    chmod 777 /var/www
    chmod 777 /var/www/html
  EOC
end

#%w{
#  /var/www
#  /var/www/html
#}.each do |file_path|
#  file file_path do
#    mode '0777'
#    action :create
#    only_if { ::File.exists?(file_path) } # ファイルが無い時には作成したくない場合に付ける
#  end # contentがnilならfileの中身は変更されない
#end

# ssh設定を変更 rootでログイン禁止等
#template "sshd_config" do
#  owner  "root"
#  group  "root"
#  mode   "0644"
#  path   "/etc/ssh/sshd_config"
#  source "sshd_config.erb"
#end

#service "sshd" do
#  action :restart
#end
