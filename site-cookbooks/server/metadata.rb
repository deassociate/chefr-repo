name             'server'
maintainer       'Tsukuru Tanimichi'
maintainer_email 'public.tanimichi@gmail.com'
license          'MIT'
description      'a cookbook for server'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
