
service "iptables" do
  action [:stop, :disable]
end

%w{git emacs}.each do |pkg|
  package pkg do
    action :install
  end
end

directory "/home/vagrant/.emacs.d" do
  owner  "vagrant"
  group  "vagrant"
  action :create
end

template "init.el" do
  owner  "vagrant"
  group  "vagrant"
  mode   "0644"
  path   "/home/vagrant/.emacs.d/init.el"
  source "init.el.erb"
end

package "nginx" do
  action :install
end

service "nginx" do
  supports :status => true, :restart => true, :reload => true
  action [ :enable , :start ]
end

git "/home/vagrant/.rbenv" do
  repository "https://github.com/sstephenson/rbenv.git"
  revision   "master"
  user       "vagrant"
  group      "vagrant"
  action     :sync
end

directory "/home/vagrant/.rbenv/plugins" do
  owner  "vagrant"
  group  "vagrant"
  action :create
end

git "/home/vagrant/.rbenv/plugins/ruby-build" do
  repository "https://github.com/sstephenson/ruby-build.git"
  revision   "master"
  user       "vagrant"
  group      "vagrant"
  action     :sync
end

%w{.bash_profile .bashrc}.each do |filename|
  template filename do
    owner  "vagrant"
    group  "vagrant"
    mode   "0644"
    path   "/home/vagrant/#{filename}"
    source "#{filename}.erb"
  end
end

template "rbenv.sh" do
  owner  "vagrant"
  group  "vagrant"
  path   "/home/vagrant/rbenv.sh"
  source "rbenv.sh.erb"
end

package "openssl-devel" do
  action :install
end

bash "rbenv install 2.0.0-p353" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; rbenv install 2.0.0-p353"
  action :run
  not_if { ::File.exists? "/home/vagrant/.rbenv/versions/2.0.0-p353" }
end

bash "rbenv rehash" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; rbenv rehash"
  action :run
end

bash "rbenv global 2.0.0-p353" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; rbenv global 2.0.0-p353"
  action :run
end

bash "bundler" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; gem install bundler"
  action :run
  not_if { ::File.exists? "/home/vagrant/.rbenv/shims/bundle" }
end

bash "unicorn" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; gem install unicorn"
  action :run
  not_if { ::File.exists? "/home/vagrant/.rbenv/shims/unicorn" }
end

bash "rbenv rehash" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; rbenv rehash"
  action :run
end
