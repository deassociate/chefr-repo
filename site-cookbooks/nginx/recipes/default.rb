
# RPMファイル取得 epel
remote_file "#{Chef::Config[:file_cache_path]}/epel-release-6-8.noarch.rpm" do
  source "http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm"
  action :create
  notifies :install, "rpm_package[epel-release]", :immediately
end

# RPMファイルインストール epel 
rpm_package "epel-release" do
  source "#{Chef::Config[:file_cache_path]}/epel-release-6-8.noarch.rpm"
  action :nothing
end

# RPMファイル取得 remi
remote_file "#{Chef::Config[:file_cache_path]}/remi-release-5.rpm" do
  source "http://rpms.famillecollet.com/enterprise/remi-release-5.rpm"
  action :create
  notifies :install, "rpm_package[epel-release]", :immediately
end

# RPMファイルインストール epel 
rpm_package "remi-release" do
  source "#{Chef::Config[:file_cache_path]}/remi-release-5.rpm"
  action :nothing
end

# nginxインストール
package "nginx" do
  action :install
end

# FastCGIやらPHP関連のものをインストール
%w{php ImageMagick ImageMagick-devel php-mbstring php-mysql php-pdo php-mcrypt php-soap php-xmlrpc php-pecl-apc php-pecl-imagick php-pecl-memcache }.each do |pkg|
  package pkg do
    action :install
  end
end

# spawn-fcgiのインストール
package "spawn-fcgi" do
  action :install
end

# phpMyAdminのセッション領域アクセス解放
bash "php session access change" do
  user   "root"
  cwd    "/home"
  code   "chmod 777 /var/lib/php/session"
  action :run
end

#%w{
#  /var/lib/php/session
#}.each do |file_path|
#  file file_path do
#    mode '0777'
#    action :create
#    only_if { ::File.exists?(file_path) } # ファイルが無い時には作成したくない場合に付ける
#  end # contentがnilならfileの中身は変更されない
#end

# nginx設定を変更
template "nginx.conf" do
  owner  "root"
  group  "root"
  mode   "0777"
  path   "/etc/nginx/nginx.conf"
  source "nginx.erb"
end

template "/etc/nginx/conf.d/phpmyadmin.conf" do
  source "phpmyadmin.conf.erb"
  mode   "0777"
  owner "root"
  group "root"
  action :create
end

# FAST-CGI起動シェルを設置
template "php-fastcgi" do
  owner  "root"
  group  "root"
  mode   "0777"
  path   "/etc/rc.d/init.d/php-fastcgi"
  source "php-fastcgi.erb"
end

# nginx 起動
service "nginx" do
  supports :status => true, :restart => true, :reload => true
  action [ :enable , :start ]
end

# FAST-CGIサービスに登録
bash "chkconfig php-fastcgi on" do
  user   "root"
  cwd    "/sbin"
  code   "/sbin/chkconfig --add php-fastcgi; /sbin/chkconfig php-fastcgi on"
  action :run
end

# FAST-CGIサービス起動
service "php-fastcgi" do
  supports :status => true, :restart => true, :reload => true
  action [ :enable , :start ]
end

